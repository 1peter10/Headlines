#include "ImagePage.h"

#include <utility>
#include <Application.h>
#include "AppSettings.h"
#include "util/CancellationToken.h"
#include "util/ImageCache.h"
#include "util/Helpers.h"
#include "UI/Widgets/ImageViewer.h"
#include "UI/Widgets/HeaderBar.h"

namespace ui
{
    ImagePage::ImagePage(const std::vector<util::ImageCollection>& images, const std::string& title)
        : Page(PageType::ImagePage)
        , m_Title(title)
        , m_Images(images)
        , m_OriginalPictureWidth(-1)
        , m_OriginalPictureHeight(-1)
    {

    }

    Gtk::Box*  ImagePage::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/image.ui");
        m_Notebook = builder->get_widget<Gtk::Notebook>("Notebook");
        for (const util::ImageCollection& imageCollection : m_Images)
        {
            if (imageCollection.m_Images.empty())
            {
                continue;
            }

            auto image_viewer = new ui::ImageViewer();
            m_Notebook->append_page(*image_viewer);

            const util::ImageData& imageData = imageCollection.GetImageData(util::ImageQuality::Highest);
            m_ImageFilePaths.push_back(util::ImageCache::get_image_disk_path(imageData.m_Url));
            m_cancellation_token = new util::CancellationToken();
            util::ImageCache::get()->get_image(imageData.m_Url, [this, image_viewer](const std::string& imagePath)
            {

                Glib::RefPtr<Gdk::Texture> texture = Gdk::Texture::create_from_file(Gio::File::create_for_path(imagePath));
                image_viewer->set_texture(texture);
                m_ImageDownloaded = true;
                if (m_Spinner)
                {
                    m_Spinner->stop();
                    m_Spinner->hide();
                }
                m_cancellation_token = nullptr;
            }, m_cancellation_token);
        }

        Gtk::Button* leftButton = builder->get_widget<Gtk::Button>("ButtonLeft");
        leftButton->set_visible(m_Images.size() > 1);
        leftButton->signal_clicked().connect([this]()
        {
            int imageIndex = m_Notebook->get_current_page();
            imageIndex--;
            if (imageIndex < 0)
            {
                imageIndex = m_Notebook->get_n_pages() - 1;
            }

            m_Notebook->set_current_page(imageIndex);
        });
        Gtk::Button* rightButton = builder->get_widget<Gtk::Button>("ButtonRight");
        rightButton->set_visible(m_Images.size() > 1);
        rightButton->signal_clicked().connect([this]()
        {
            int imageIndex = m_Notebook->get_current_page();
            imageIndex++;
            if (imageIndex >= m_Notebook->get_n_pages())
            {
                imageIndex = 0;
            }

            m_Notebook->set_current_page((int)imageIndex);
        });

        m_Spinner = builder->get_widget<Gtk::Spinner>("Spinner");
        if (m_ImageDownloaded)
        {
            m_Spinner->stop();
            m_Spinner->hide();
        }

        Gtk::Box* box = builder->get_widget<Gtk::Box>("Box");
        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        return box;
    }

    UISettings ImagePage::GetUISettings() const
    {
        return UISettings
        {
            true,
            false,
            false,
            true,
            false,
            false,
            true,
            false
        };
    }

    void ImagePage::Cleanup()
    {
        if (m_cancellation_token)
        {
            m_cancellation_token->cancel();
        }
    }

    void ImagePage::OnSaveButton()
    {
        auto fileChooser = Gtk::FileChooserNative::create("Save Image...", Gtk::FileChooserDialog::Action::SAVE, "Save", "Cancel");
        fileChooser->set_current_name(util::Helpers::GetFileName(m_ImageFilePaths[m_Notebook->get_current_page()]));
        Glib::RefPtr<Gio::File> homeFolder = Gio::File::create_for_path(std::getenv("HOME"));
        fileChooser->set_current_folder(homeFolder);
        fileChooser->show();
        fileChooser->signal_response().connect([this, fileChooser](int response)
        {
            if ((Gtk::ResponseType)response == Gtk::ResponseType::ACCEPT)
            {
                std::filesystem::copy(m_ImageFilePaths[m_Notebook->get_current_page()], fileChooser->get_file()->get_path(), std::filesystem::copy_options::overwrite_existing);
            }

            fileChooser->hide();
        });
        fileChooser->set_transient_for(*Application::Get()->GetGtkApplication()->get_active_window());
    }

    void ImagePage::OnHeaderBarCreated()
    {
        GetHeaderBar()->SetTitle(m_Title);
    }
}

#include "SubredditWidget.h"
#include "AppSettings.h"
#include "util/Helpers.h"
#include "API/RedditAPI.h"
#include "API/Subreddit.h"
#include "util/HtmlParser.h"
#include "UI/Widgets/PictureWidget.h"

namespace ui
{
    SubredditWidget::~SubredditWidget()
    {
        if (m_RoundedCornersSettingChangedHandler != util::s_InvalidSignalHandlerID)
        {
            util::Signals::Get()->RoundedCornersSettingChanged.RemoveHandler(m_RoundedCornersSettingChangedHandler);
        }
    }

    void SubredditWidget::CreateUI(Gtk::Box* parent, bool prepend)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/subreddit.ui");

        m_SubredditPanel = builder->get_widget<Gtk::Box>("SubredditPanel");
        m_SubRedditLabel = builder->get_widget<Gtk::Label>("SubredditLabel");
        m_SubCountLabel = builder->get_widget<Gtk::Label>("SubCountLabel");
        m_CreatedLabel = builder->get_widget<Gtk::Label>("CreatedDateLabel");
        m_DescriptionBox = builder->get_widget<Gtk::Box>("DescriptionContainer");
        m_SubscribeButton = builder->get_widget<Gtk::ToggleButton>("SubscribeButton");
        m_SubscribeButton->set_sensitive(api::RedditAPI::Get()->IsLoggedIn());

        bool rounded = AppSettings::Get()->GetBool("rounded_corners", false);
        util::Helpers::ApplyCardStyle(m_SubredditPanel, rounded);
        m_RoundedCornersSettingChangedHandler = util::Signals::Get()->RoundedCornersSettingChanged.AddHandler([this](bool roundedCorners)
        {
            util::Helpers::ApplyCardStyle(m_SubredditPanel, roundedCorners);
        });

        Gtk::Box* subIconBox;
        subIconBox = builder->get_widget<Gtk::Box>("SubIconBox");
        Gtk::Requisition minSize, preferredSize;
        subIconBox->get_preferred_size(minSize, preferredSize);
        m_IconImageWidget = new PictureWidget(preferredSize.get_width(), preferredSize.get_height(), false, false);
        subIconBox->append(*m_IconImageWidget);

        if (prepend )
        {
            parent->prepend(*m_SubredditPanel);
        }
        else
        {
            parent->append(*m_SubredditPanel);
        }
    }

    void SubredditWidget::SetSubreddit(const api::SubredditRef& subreddit)
    {
        m_Subreddit = subreddit;

        m_SubRedditLabel->set_text(m_Subreddit->GetTitle());
        m_SubscribeButton->set_active(m_Subreddit->UserIsSubscriber());
        m_SubscribeButton->set_label(m_SubscribeButton->get_active() ? "Unsubscribe" : "Subscribe");
        m_SubscribeButton->signal_toggled().connect([this]()
        {
            m_Subreddit->SetUserIsSubscriber(m_SubscribeButton->get_active());
            if (m_SubscribeButton->get_active())
            {
                api::RedditAPI::Get()->Subscribe(m_Subreddit);
                m_SubscribeButton->set_label("Unsubscribe");
            }
            else
            {
                api::RedditAPI::Get()->UnSubscribe(m_Subreddit);
                m_SubscribeButton->set_label("Subscribe");
            }

        });

        {
            std::stringstream ss;
            ss << "Subscribers: " << m_Subreddit->GetSubscribers();

            m_SubCountLabel->set_text(ss.str());
        }

        {
            std::stringstream ss;
            ss << "Created: " << util::Helpers::TimeStampToDateString(m_Subreddit->GetCreatedTimeStamp());

            m_CreatedLabel->set_text(ss.str());
        }

        util::HtmlParser htmlParser;
        htmlParser.ParseHtml(m_Subreddit->GetPublicDesc(), m_DescriptionBox, -1, true);

        m_IconImageWidget->SetImageData(util::ImageData(subreddit->GetIconPath()));

        m_SubredditPanel->set_visible(true);
    }

    void SubredditWidget::SetVisible(bool visible)
    {
        m_SubredditPanel->set_visible(visible);
    }
}
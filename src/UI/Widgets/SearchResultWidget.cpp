#include "SearchResultWidget.h"
#include <utility>
#include <Application.h>
#include "API/User.h"
#include "API/Subreddit.h"
#include "API/MultiReddit.h"
#include "AppSettings.h"
#include "UI/Widgets/PictureWidget.h"
#include "API/RedditAPI.h"

namespace ui
{
    SearchResultWidget::SearchResultWidget(const api::UserRef& user, std::function<void(const SearchResultWidget*)> onClicked)
            : RedditContentListItem()
            , m_ImageUrl(user->GetIconPath())
            , m_DisplayName(user->GetDisplayNamePrefixed())
            , m_User(user)
            , m_Subreddit(nullptr)
            , m_Box(nullptr)
            , m_Label(nullptr)
            , m_OnClicked(std::move(onClicked))
    {
    }

    SearchResultWidget::SearchResultWidget(const api::SubredditRef& subreddit, std::function<void(const SearchResultWidget*)> onClicked)
        : RedditContentListItem()
        , m_ImageUrl(subreddit->GetIconPath())
        , m_DisplayName(subreddit->GetDisplayName())
        , m_User(nullptr)
        , m_Subreddit(subreddit)
        , m_Box(nullptr)
        , m_Label(nullptr)
        , m_OnClicked(std::move(onClicked))
    {
    }


    SearchResultWidget::SearchResultWidget(const api::MultiRedditRef& multiReddit, std::function<void(const SearchResultWidget*)> onClicked)
        : RedditContentListItem()
          , m_ImageUrl(multiReddit->GetIconUrl())
          , m_DisplayName(multiReddit->GetDisplayName())
          , m_User(nullptr)
          , m_MultiReddit(multiReddit)
          , m_Box(nullptr)
          , m_Label(nullptr)
          , m_OnClicked(std::move(onClicked))
    {

    }

    SearchResultWidget::~SearchResultWidget()
    {
    }

    void SearchResultWidget::RemoveUI()
    {
        if (m_ListBoxRow)
        {
            Gtk::ListBox* box = (Gtk::ListBox*) m_ListBoxRow->get_parent();
            box->remove(*m_ListBoxRow);
            m_ListBoxRow = nullptr;
        }
    }

    void SearchResultWidget::CreateUI(Gtk::Widget* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/search_result.ui");

        m_Box = builder->get_widget<Gtk::Box>("SearchResultBox");
        m_Box->set_vexpand(false);

        Gtk::Box* imageBox;
        imageBox = builder->get_widget<Gtk::Box>("ImageBox");

        m_Label = builder->get_widget<Gtk::Label>("SearchResultLabel");
        m_Label->set_text(m_DisplayName);

        m_Icon = new PictureWidget(32, 32, false, false);
        imageBox->append(*m_Icon);
        m_Icon->SetImageData(util::ImageData(m_ImageUrl));

        m_Box->set_margin_start(10);
        m_Box->set_margin_end(10);

        if (Gtk::ListBox* listBox = dynamic_cast<Gtk::ListBox*>(parent))
        {
            m_ListBoxRow = new Gtk::ListBoxRow();
            m_ListBoxRow->set_child(*m_Box);
            m_ListBoxRow->set_name(m_DisplayName);

            listBox->append(*m_ListBoxRow);
        }
        else if (Gtk::Box* boxParent = dynamic_cast<Gtk::Box*>(parent))
        {
            Glib::RefPtr<Gtk::GestureClick> gestureClick = Gtk::GestureClick::create();
            gestureClick->signal_released().connect([this](int n_press, double, double)
            {
                if (n_press == 0)
                {
                    return;
                }

                if (m_OnClicked)
                {
                    m_OnClicked(this);
                }
            });
            m_Box->add_controller(gestureClick);
            boxParent->append(*m_Box);
        }
        else
        {
            g_warning("SearchResultWidget::CreateUI - unhandled parent type.");
        }

        if (m_MultiReddit)
        {
            AddActionButton("user-trash-symbolic", [this]()
            {
                Gtk::MessageDialog* confirmDialog = new Gtk::MessageDialog("Are you sure you want to delete this Multireddit?", false, Gtk::MessageType::WARNING, Gtk::ButtonsType::OK_CANCEL);
                confirmDialog->signal_response().connect([this, confirmDialog](int response)
                {
                    if (response == Gtk::ResponseType::OK)
                    {
                        api::RedditAPI::Get()->DeleteMultiReddit(m_MultiReddit);
                        RemoveUI();

                        Application::Get()->ShowNotification("Multi deleted.");
                    }

                    delete confirmDialog;
                });

                confirmDialog->set_transient_for(*Application::Get()->GetGtkApplication()->get_active_window());
                confirmDialog->show();

            });
        }

    }

    void SearchResultWidget::SetSubreddit(const api::SubredditRef& subreddit)
    {
        m_Subreddit = subreddit;
        m_Label->set_text(subreddit->GetDisplayName());
        m_Icon->SetImageData(util::ImageData(subreddit->GetIconPath()));
    }

    int SearchResultWidget::GetContentTop()
    {
        return m_ListBoxRow->get_allocation().get_y();
    }

    int SearchResultWidget::GetContentBottom()
    {
        const Gtk::Allocation& allocation = m_ListBoxRow->get_allocation();
        return allocation.get_y() + allocation.get_height();
    }

    void SearchResultWidget::AddActionButton(const std::string& iconName, std::function<void()> cb)
    {
        Gtk::Button* button = new Gtk::Button();
        button->set_icon_name(iconName);
        button->signal_clicked().connect(cb);
        button->set_halign(Gtk::Align::END);
        button->set_valign(Gtk::Align::CENTER);
        button->set_hexpand(true);
        m_Box->append(*button);
    }
}

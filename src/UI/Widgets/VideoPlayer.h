#pragma once

namespace ui
{
    class VideoPlayer : public Gtk::Widget
    {
    public:
        VideoPlayer(const std::string video_url);
        ~VideoPlayer() override;

        void play();
        void pause();

        void set_controls_visible(bool visible, bool addHideTimeout);
    private:
        virtual void size_allocate_vfunc(int width, int height, int baseline) override;
        virtual void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        virtual void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;

        void setup_for_youtube(const std::string& url);
        void setup_for_dash(const std::string& url);

        void on_video_url_ready();
        void setup_duration(double seconds);

        xmlDocPtr get_dash_manifest();
        static std::string get_time_str_from_dash_manifest(xmlDocPtr dashDoc);
        static void parse_dash_time_string(std::string timeString, double& seconds);

        std::string m_url;
        bool m_controls_visible = false;
        sigc::connection m_timeout_connection;

        std::thread* m_url_prep_thread = nullptr;
        Glib::Dispatcher m_video_url_ready_dispatcher;

        Gtk::Label* m_curr_time_label = nullptr;
        Gtk::Label* m_duration_label = nullptr;
        Gtk::Scale* m_seek_bar = nullptr;
        Gtk::Picture* m_video = nullptr;
        Gtk::Revealer* m_controls_revealer = nullptr;
        Gtk::Button* m_play_button = nullptr;
        Gtk::Button* m_mute_button = nullptr;
        Gtk::Button* m_fullscreen_button = nullptr;
        Gtk::Spinner* m_spinner = nullptr;
        Glib::RefPtr<Gtk::MediaFile> m_media_file = nullptr;

        sigc::connection m_duration_connection;

        double m_last_pointer_x;
        double m_last_pointer_y;
    };
}

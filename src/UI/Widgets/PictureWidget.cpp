#include <Application.h>
#include "PictureWidget.h"
#include "AppSettings.h"
#include "util/SimpleThread.h"
#include "util/ThreadWorker.h"
#include "util/ImageCache.h"
#include "util/CancellationToken.h"

namespace ui
{
    PictureWidget::PictureWidget(int imageWidth, int imageHeight, bool crop, bool hide)
            : m_Crop(crop && AppSettings::Get()->GetBool("crop_images", true))
            , m_Hide(hide)
    {
        m_AspectRatio = (float) imageWidth / imageHeight;

        if (m_Crop)
        {
            m_AspectRatio = std::max(m_AspectRatio, 2.f);
        }

        m_Spinner = new Gtk::Spinner();
        m_Spinner->set_size_request(std::min(100, imageWidth), std::min(100, imageHeight));
        m_Spinner->set_halign(Gtk::Align::CENTER);
        m_Spinner->set_valign(Gtk::Align::CENTER);
        m_Spinner->set_parent(*this);
        m_Spinner->start();
    }

    PictureWidget::~PictureWidget()
    {
        //intentionally not deleted, image cache will delete it.
        if (m_cancellation_token)
        {
            m_cancellation_token->cancel();
        }
    }

    void PictureWidget::SetFilePath(const std::string& filepath)
    {
        m_FilePath = filepath;
        try
        {
            m_Pixbuf = Gdk::Pixbuf::create_from_file(m_FilePath);

            //sometimes reddit lies about the size of images.
            if (!m_Crop || m_AspectRatio > 2)
            {
                float realAspect = (float) m_Pixbuf->get_width() / m_Pixbuf->get_height();
                if (std::abs(realAspect - m_AspectRatio) > FLT_EPSILON)
                {
                    m_AspectRatio = realAspect;
                    queue_resize();
                }
            }

            if (m_Crop)
            {
                CropImage();
            }
            m_Texture = Gdk::Texture::create_for_pixbuf(m_Pixbuf);
            m_Spinner->stop();
        }
        catch (const Glib::Error& error)
        {
            std::filesystem::remove(filepath);
            g_info("%s %s", error.what(), filepath.c_str());

            if (++m_DownloadRetryCount < 10)
            {
                Application::Get()->run_on_ui_thread([this]()
                {
                    SetImageData(m_ImageData);
                });
            }
        }
        queue_draw();
    }

    void PictureWidget::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_Child)
        {
            m_Child->size_allocate(rect, baseline);
        }

        m_Spinner->size_allocate(rect, baseline);
    }

    void PictureWidget::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int&, int&) const
    {
        if (orientation == Gtk::Orientation::VERTICAL)
        {
            if (get_halign() == Gtk::Align::FILL)
            {
                natural = for_size / m_AspectRatio;
            }
        }

        if (m_Child)
        {
            int childMinimum, childNatural, childMinBaseLine, childNatBaseline;
            m_Child->measure(orientation, for_size, childMinimum, childNatural, childMinBaseLine, childNatBaseline);
            minimum = std::max(minimum, childMinimum);
        }

        int childMinimum, childNatural, childMinBaseLine, childNatBaseline;
        m_Spinner->measure(orientation, for_size, childMinimum, childNatural, childMinBaseLine, childNatBaseline);
        minimum = std::max(minimum, childMinimum);

        natural = std::max(natural, minimum);
    }

    void PictureWidget::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        bool useBlurEffect = AppSettings::Get()->GetBool("use_blur_effect", false);

        if (m_Hide)
        {
            Gdk::Rectangle rect = Gdk::Rectangle(0, 0, get_width(), get_height());
            if (useBlurEffect)
            {
                snapshot->push_clip(rect);
                snapshot->push_blur(AppSettings::Get()->GetInt("blur_radius", 50));
            }
            else
            {
                snapshot->append_color(Gdk::RGBA(0, 0, 0), rect);
            }
        }

        if (m_Texture && (!m_Hide || useBlurEffect))
        {
            m_Texture->snapshot(snapshot, get_allocated_width(), get_allocated_height());
        }

        if (m_Hide && useBlurEffect)
        {
            snapshot->pop();
            snapshot->pop();
        }

        if (m_Child)
        {
            snapshot_child(*m_Child, snapshot);
        }

        snapshot_child(*m_Spinner, snapshot);
    }

    Gtk::SizeRequestMode PictureWidget::get_request_mode_vfunc() const
    {
        return Gtk::SizeRequestMode::WIDTH_FOR_HEIGHT;
    }

    void PictureWidget::SetFromResource(const std::string& resourcePath)
    {
        m_ResourcePath = resourcePath;
        m_Pixbuf = Gdk::Pixbuf::create_from_resource(resourcePath);

        //sometimes reddit lies about the size of images.
        if (!m_Crop)
        {
            float realAspect = (float) m_Pixbuf->get_width() / m_Pixbuf->get_height();
            if (std::abs(realAspect - m_AspectRatio) > FLT_EPSILON)
            {
                m_AspectRatio = realAspect;
                queue_resize();
            }
        }

        if (m_Crop)
        {
            CropImage();
        }
        m_Texture = Gdk::Texture::create_for_pixbuf(m_Pixbuf);
        m_Spinner->stop();
        queue_draw();
    }

    void PictureWidget::SetImageData(const util::ImageData& imageData)
    {
        m_ImageData = imageData;

        if (m_ImageData.m_Url.find("/io/gitlab/caveman250/headlines") == 0)
        {
            SetFromResource(m_ImageData.m_Url);
            return;
        }

        if (m_ImageData.m_Url.empty())
        {
            SetFromResource("/io/gitlab/caveman250/headlines/default_subreddit.png");
            return;
        }

        m_cancellation_token = new util::CancellationToken();
        util::ImageCache::get()->get_image(m_ImageData.m_Url, [this](const std::string& filePath)
        {
            SetFilePath(filePath);
            m_cancellation_token = nullptr;
        }, m_cancellation_token);
    }

    void PictureWidget::SetClickHandler(std::function<void()> clickhandler)
    {
        get_style_context()->add_class("image");
        m_OnClick = clickhandler;

        Glib::RefPtr<Gtk::GestureClick> gestureClick = Gtk::GestureClick::create();
        add_controller(gestureClick);
        gestureClick->signal_released().connect([this](int n_press, double, double)
        {
            if (n_press == 0)
                return;

            m_OnClick();
        });
    }

    void PictureWidget::CropImage()
    {
        int heightDiff = m_Pixbuf->get_height() - m_Pixbuf->get_width() / m_AspectRatio;

        if (heightDiff > 0)
        {
            m_Pixbuf = Gdk::Pixbuf::create_subpixbuf(m_Pixbuf, 0, heightDiff / 2, m_Pixbuf->get_width(), m_Pixbuf->get_width() / m_AspectRatio);
        }
    }

    void PictureWidget::SetActive(bool active)
    {
        if (!active)
        {
            if (m_cancellation_token)
            {
                m_cancellation_token->cancel();
            }

            m_Pixbuf.reset();
            m_Texture.reset();
            m_Spinner->start();
        }
        else if (!m_Pixbuf)
        {
            queue_resize();
            if (!m_ResourcePath.empty())
            {
                SetFromResource(m_ResourcePath);
            }
            else
            {
                SetImageData(m_ImageData);
            }
        }
    }
}

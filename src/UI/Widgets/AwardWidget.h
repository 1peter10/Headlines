#pragma once

#include "pch.h"
#include "API/api_fwd.h"

namespace ui
{
    class AwardWidget : public Gtk::Widget
    {
    public:
        AwardWidget(const api::Award& award);
    private:
        virtual void size_allocate_vfunc(int width, int height, int baseline) override;
        virtual void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        virtual void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;
        virtual Gtk::SizeRequestMode get_request_mode_vfunc() const override;

        Gtk::Box* m_Child;
    };
}

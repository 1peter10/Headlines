#pragma once

namespace ui
{
    class GifWidget : public Gtk::Widget
    {
    public:
        GifWidget(int width, int height);
        virtual ~GifWidget();

        const Glib::RefPtr<Gdk::PixbufAnimation>& get_animation() const { return m_animation; }
        const Glib::RefPtr<Gdk::PixbufAnimationIter>& get_iter() const { return m_iter; }
        Gtk::Picture* get_picture() const { return m_child; }

        void set_gif(const std::string& file_path);
        void play(bool once);
        void pause();

    private:

        void size_allocate_vfunc(int width, int height, int baseline) override;
        void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        void snapshot_vfunc(const Glib::RefPtr <Gtk::Snapshot>& snapshot) override;
        Gtk::SizeRequestMode get_request_mode_vfunc() const override;

        float m_aspect_ratio = 1;
        Glib::RefPtr<Gdk::PixbufAnimation> m_animation;
        Glib::RefPtr<Gdk::PixbufAnimationIter> m_iter;
        Gtk::Image* m_play_button;
        Gtk::Picture* m_child;
        Gtk::Spinner* m_spinner;
        sigc::connection m_timeout_connection;
        bool m_paused = true;
    };
}


#pragma once

#include "API/api_fwd.h"
#include "util/Signals.h"
#include "UI/ui_types_fwd.h"

namespace ui
{
    class PictureWidget;
    class UserWidget
    {
    public:
        ~UserWidget();
        void CreateUI(Gtk::Box* parent);
        void SetUser(const api::UserRef& user);
        void SetVisible(bool visible);

    private:
        api::UserRef m_User = nullptr;

        PictureWidget* m_IconImageWidget = nullptr;
        Gtk::Box* m_UserPanel = nullptr;
        Gtk::Label* m_UserLabel = nullptr;
        Gtk::Label* m_KarmaLabel = nullptr;
        Gtk::Label* m_CakeDayLabel = nullptr;
        Gtk::ToggleButton* m_FollowButton = nullptr;

        int m_RoundedCornersSettingChangedHandler = util::s_InvalidSignalHandlerID;
    };
}

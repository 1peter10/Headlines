#include <util/HttpClient.h>
#include <Application.h>
#include "VideoPlayer.h"
#include "HeaderBar.h"

namespace ui
{
    std::string DurationToString(double seconds)
    {
        int hours = (int)(seconds / 60 / 60);
        seconds -= (double)hours * 60 * 60;
        int minutes = (int)(seconds / 60);
        seconds -= (double)minutes * 60;

        char buf[1024];
        if (hours > 0)
        {
            snprintf(buf, 1024, "<span foreground=\"#FFFFFF\">%d:%02d:%02.0f</span>", hours, minutes, seconds);
        }
        else
        {
            snprintf(buf, 1024, "<span foreground=\"#FFFFFF\">%02d:%02.0f</span>", minutes, seconds);
        }

        return std::string((const char*)&buf);
    }

    VideoPlayer::VideoPlayer(const std::string video_url)
    {
        m_video_url_ready_dispatcher.connect([this]()
        {
            on_video_url_ready();
        });

        add_css_class("video_player");

        m_video = new Gtk::Picture();
        m_video->set_parent(*this);

        m_controls_revealer = new Gtk::Revealer();
        m_controls_revealer->set_transition_type(Gtk::RevealerTransitionType::SLIDE_UP);
        m_controls_revealer->set_valign(Gtk::Align::END);
        m_controls_revealer->set_parent(*this);

        GtkWidget* clamp = adw_clamp_new();
        adw_clamp_set_maximum_size((AdwClamp*)clamp, 900);
        gtk_revealer_set_child(m_controls_revealer->gobj(), clamp);

        auto controls_box = new Gtk::Box(Gtk::Orientation::VERTICAL);
        controls_box->set_hexpand(true);
        controls_box->set_valign(Gtk::Align::END);
        controls_box->set_margin_start(20);
        controls_box->set_margin_end(20);
        controls_box->set_margin_bottom(20);
        controls_box->add_css_class("video-controls");
        adw_clamp_set_child((AdwClamp*)clamp, (GtkWidget*)controls_box->gobj());

        auto buttons_box = new Gtk::Box(Gtk::Orientation::HORIZONTAL);
        buttons_box->set_margin_top(10);
        controls_box->append(*buttons_box);

        auto seek_backwards_button = new Gtk::Button();
        seek_backwards_button->set_hexpand(true);
        seek_backwards_button->set_has_frame(false);
        seek_backwards_button->set_margin_start(10);
        seek_backwards_button->set_icon_name("media-seek-backward-symbolic");
        ((Gtk::Image*)(seek_backwards_button->get_child()))->set_pixel_size(48);
        seek_backwards_button->add_css_class("video-control");
        seek_backwards_button->signal_clicked().connect([this]()
        {
            m_media_file->seek(std::max(m_media_file->get_timestamp() - (G_USEC_PER_SEC * 10), (gint64)0));
            set_controls_visible(true, m_media_file->get_playing());
        });
        buttons_box->append(*seek_backwards_button);

        m_play_button = new Gtk::Button();
        m_play_button->set_hexpand(true);
        m_play_button->set_has_frame(false);
        m_play_button->set_icon_name("media-playback-start-symbolic");
        ((Gtk::Image*)(m_play_button->get_child()))->set_pixel_size(48);
        m_play_button->add_css_class("video-control");
        m_play_button->signal_clicked().connect([this]()
        {
            m_media_file->get_playing() ? m_media_file->pause() : m_media_file->play();
            set_controls_visible(true, m_media_file->get_playing());
        });
        buttons_box->append(*m_play_button);

        auto seek_forwards_button = new Gtk::Button();
        seek_forwards_button->set_hexpand(true);
        seek_forwards_button->set_has_frame(false);
        seek_forwards_button->set_icon_name("media-seek-forward-symbolic");
        ((Gtk::Image*)(seek_forwards_button->get_child()))->set_pixel_size(48);
        seek_forwards_button->add_css_class("video-control");
        seek_forwards_button->signal_clicked().connect([this]()
        {
            m_media_file->seek(std::min(m_media_file->get_timestamp() + (G_USEC_PER_SEC * 10), m_media_file->get_duration() - 10));
            set_controls_visible(true, m_media_file->get_playing());
        });
        buttons_box->append(*seek_forwards_button);

        m_mute_button = new Gtk::Button();
        m_mute_button->set_hexpand(true);
        m_mute_button->set_has_frame(false);
        m_mute_button->set_icon_name("audio-volume-high-symbolic");
        ((Gtk::Image*)(m_mute_button->get_child()))->set_pixel_size(48);
        m_mute_button->add_css_class("video-control");
        m_mute_button->signal_clicked().connect([this]()
        {
            bool wasMuted = m_media_file->get_muted();
            m_media_file->set_muted(!wasMuted);
            m_mute_button->set_icon_name(wasMuted ? "audio-volume-high-symbolic" : "audio-volume-muted-symbolic");
            set_controls_visible(true, m_media_file->get_playing());
        });
        buttons_box->append(*m_mute_button);

        m_fullscreen_button = new Gtk::Button();
        m_fullscreen_button->set_hexpand(true);
        m_fullscreen_button->set_has_frame(false);
        m_fullscreen_button->set_icon_name("view-fullscreen-symbolic");
        ((Gtk::Image*)(m_fullscreen_button->get_child()))->set_pixel_size(48);
        m_fullscreen_button->add_css_class("video-control");
        m_fullscreen_button->signal_clicked().connect([this]()
        {
            auto window = Application::Get()->GetWindow();
            if (window->is_fullscreen())
            {
                m_fullscreen_button->set_icon_name("view-fullscreen-symbolic");
                window->unfullscreen();

                AdwFlap* flap = Application::Get()->GetFlap();
                GtkWidget* flap_widget = adw_flap_get_flap(flap);
                gtk_widget_set_visible(flap_widget, true);

                Application::Get()->GetActivePage()->GetHeaderBar()->GetHeaderBar()->set_visible(true);
                adw_flap_set_swipe_to_open(Application::Get()->GetFlap(), true);
                adw_leaflet_set_can_navigate_back(Application::Get()->GetLeaflet(), true);
            }
            else
            {
                m_fullscreen_button->set_icon_name("view-restore-symbolic");
                window->fullscreen();

                AdwFlap* flap = Application::Get()->GetFlap();
                GtkWidget* flap_widget = adw_flap_get_flap(flap);
                gtk_widget_set_visible(flap_widget, false);

                Application::Get()->GetActivePage()->GetHeaderBar()->GetHeaderBar()->set_visible(false);
                adw_flap_set_swipe_to_open(Application::Get()->GetFlap(), false);
                adw_leaflet_set_can_navigate_back(Application::Get()->GetLeaflet(), false);
            }
        });
        buttons_box->append(*m_fullscreen_button);

        auto seek_bar_box = new Gtk::Box(Gtk::Orientation::HORIZONTAL);
        seek_bar_box->set_margin_start(10);
        seek_bar_box->set_margin_end(10);
        seek_bar_box->set_margin_bottom(10);
        controls_box->append(*seek_bar_box);

        m_curr_time_label = new Gtk::Label();
        m_curr_time_label->set_size_request(50, -1);
        m_curr_time_label->set_halign(Gtk::Align::CENTER);
        m_curr_time_label->set_markup(DurationToString(0));
        seek_bar_box->append(*m_curr_time_label);

        m_seek_bar = new Gtk::Scale();
        m_seek_bar->set_hexpand(true);
        m_seek_bar->signal_value_changed().connect([this]()
        {
            if (!m_media_file || m_seek_bar->get_value() == (double)m_media_file->get_timestamp() / G_USEC_PER_SEC)
                return;

            m_media_file->seek((gint64)(m_seek_bar->get_value() * G_USEC_PER_SEC));

            //make sure to reset the controls timeout
            set_controls_visible(true, m_media_file->get_playing());
        });
        seek_bar_box->append(*m_seek_bar);

        m_duration_label = new Gtk::Label();
        m_duration_label->set_size_request(50, -1);
        m_duration_label->set_halign(Gtk::Align::CENTER);
        m_curr_time_label->set_markup(DurationToString(0));
        seek_bar_box->append(*m_duration_label);


        m_spinner = new Gtk::Spinner();
        m_spinner->set_valign(Gtk::Align::CENTER);
        m_spinner->set_halign(Gtk::Align::CENTER);
        m_spinner->set_size_request(100, 100);
        m_spinner->add_css_class("video-control");
        m_spinner->set_parent(*this);
        m_spinner->start();

        set_controls_visible(true, false);

        auto motion = Gtk::EventControllerMotion::create();
        motion->signal_motion().connect([this](double x, double y)
        {
            if (x != m_last_pointer_x || y != m_last_pointer_y)
            {
                m_last_pointer_x = x;
                m_last_pointer_y = y;
                set_controls_visible(true, m_media_file && m_media_file->get_playing());
            }
        });
        motion->signal_leave().connect([this]()
        {
            if (!m_controls_visible)
            {
                set_controls_visible(true, m_media_file && m_media_file->get_playing());
            }
        });
        add_controller(motion);

        Glib::RefPtr<Gtk::GestureClick> gestureClick = Gtk::GestureClick::create();
        gestureClick->signal_released().connect([this] (int n_press, double, double)
        {
            if (n_press <= 0)
            {
                return;
            }

            if (m_controls_visible && m_media_file)
            {
                m_media_file->get_playing() ? m_media_file->pause() : m_media_file->play();
            }
            set_controls_visible(true, m_media_file && m_media_file->get_playing());
        });
        add_controller(gestureClick);

        std::string sanitizedUrl = util::Helpers::UnescapeHtml(video_url);
        auto it = sanitizedUrl.find('&');
        if (it != std::string::npos)
        {
            sanitizedUrl.resize(it);
        }

        if (sanitizedUrl.find("youtube") != std::string::npos || sanitizedUrl.find("youtu.be") != std::string::npos)
        {
            setup_for_youtube(sanitizedUrl);
        }
        else if(sanitizedUrl.find(".mp4") != std::string::npos || sanitizedUrl.find(".MP4") != std::string::npos)
        {
            m_url = sanitizedUrl;
            m_video_url_ready_dispatcher.emit();
        }
        else if (sanitizedUrl.find("HLSPlaylist") != std::string::npos)
        {
            m_url = sanitizedUrl;
            m_video_url_ready_dispatcher.emit();
        }
        else
        {
            setup_for_dash(sanitizedUrl);
        }
    }

    VideoPlayer::~VideoPlayer()
    {
        if (m_timeout_connection.connected())
        {
            m_timeout_connection.disconnect();
        }

        m_media_file->pause();
        m_media_file->clear();
        m_media_file.reset();
    }

    void VideoPlayer::set_controls_visible(bool visible, bool addHideTimeout)
    {
        m_controls_visible = visible;
        m_controls_revealer->set_reveal_child(visible);

        if (m_timeout_connection.connected())
        {
            m_timeout_connection.disconnect();
        }

        if (visible && addHideTimeout)
        {
            m_timeout_connection = Glib::signal_timeout().connect([this]()
            {
                set_controls_visible(false, false);
                return false;
            }, 1500);
        }
    }

    xmlDocPtr VideoPlayer::get_dash_manifest()
    {
        //streaming the file from the Gio::File doesnt work across all devices, so download it again *sigh*
        util::HttpClient httpClient;
        std::string dashPlaylist = httpClient.Get(m_url, {});
        return xmlParseMemory(dashPlaylist.c_str(), (int)dashPlaylist.length());
    }

    void VideoPlayer::parse_dash_time_string(std::string timeString, double& seconds)
    {
        int hours = 0;
        int minutes = 0;

        timeString.erase(0, 2); //remove PT
        auto it = timeString.find('H');
        if (it != std::string::npos)
        {
            hours = std::stoi(timeString.substr(0, it));
            timeString.erase(0, it + 1);
        }
        it = timeString.find('M');
        if (it != std::string::npos)
        {
            minutes = std::stoi(timeString.substr(0, it));
            timeString.erase(0, it + 1);
        }
        it = timeString.find('S');
        if (it != std::string::npos)
        {
            seconds = std::stod(timeString.substr(0, it));
            timeString.erase(0, it + 1);
        }

        seconds += minutes * 60;
        seconds += hours * 60 * 60;
    }

    std::string VideoPlayer::get_time_str_from_dash_manifest(xmlDocPtr dashDoc)
    {
        xmlNodePtr docRoot = xmlDocGetRootElement(dashDoc);
        xmlNodePtr node = util::Helpers::FindXmlNodeByName(docRoot, (const xmlChar*)"Period");
        for (xmlAttrPtr attribute = node->properties; attribute != nullptr; attribute = attribute->next)
        {
            if (strcmp((const char*)attribute->name, "duration") == 0)
            {
                return (const char*)attribute->children->content;
            }
        }

        return std::string();
    }

    void VideoPlayer::setup_duration(double seconds)
    {
        double seek_bar_upper = m_seek_bar->get_adjustment()->get_upper();
        if (seconds != seek_bar_upper)
        {
            Glib::RefPtr<Gtk::Adjustment> adjustment = Gtk::Adjustment::create(m_seek_bar->get_value(), 0, seconds);
            m_seek_bar->set_adjustment(adjustment);
            m_duration_label->set_markup(DurationToString(seconds));
        }
    }

    void VideoPlayer::setup_for_dash(const std::string& url)
    {
        m_url_prep_thread = new std::thread([this, url]()
        {
            m_url = url;

            //we need to calculate the duration of the video ourselves since gst cant work out the duration of dash playlists.
            xmlDocPtr dashDoc = get_dash_manifest();
            if (dashDoc == nullptr)
            {
                g_warning("VideoPlayer::setup_for_dash - failed to download DASH manifest %s. Aborting.", m_url.c_str());
                return;
            }
            std::string timeString = get_time_str_from_dash_manifest(dashDoc);
            double seconds = 0;
            parse_dash_time_string(timeString, seconds);
            setup_duration(seconds);

            m_video_url_ready_dispatcher.emit();
        });
    }

    void VideoPlayer::setup_for_youtube(const std::string& url)
    {
        m_url_prep_thread = new std::thread([this, url]()
        {
            std::stringstream ss;
            ss << "youtube-dl -f \"best[height<=?480][fps<=?30]\" --get-url " << url;
            FILE* youtubeDl = popen(ss.str().c_str(), "r");

            char buffer[1024];
            char* output = fgets(buffer, sizeof(buffer), youtubeDl);
            pclose(youtubeDl);

            if (output == nullptr)
            {
                g_warning("VideoPlayer::setup_for_youtube - No output from youtube-dl. aborting.");
                return;
            }

            m_url = output;
            m_video_url_ready_dispatcher.emit();
        });
    }

    void VideoPlayer::on_video_url_ready()
    {
        Glib::RefPtr<Gio::File> file = Gio::File::create_for_uri(m_url);
        m_media_file = Gtk::MediaFile::create();
        m_media_file->set_file(file);
        m_video->set_paintable(m_media_file);
        Glib::RefPtr<Gdk::Surface> surface = m_video->get_native()->get_surface();
        m_media_file->realize(surface);

        m_media_file->property_timestamp().signal_changed().connect([this]()
        {
            double timeStamp = (double)m_media_file->get_timestamp() / G_USEC_PER_SEC;
            m_seek_bar->set_value(timeStamp);
            m_curr_time_label->set_markup(DurationToString(timeStamp));
        });

        m_media_file->property_playing().signal_changed().connect([this]()
        {
            if (!m_media_file->get_playing())
            {
                set_controls_visible(true, false);
            }

            m_play_button->set_icon_name(m_media_file->get_playing() ? "media-playback-pause-symbolic" : "media-playback-start-symbolic");
        });

        m_media_file->property_ended().signal_changed().connect([this]()
        {
            set_controls_visible(true, false);
            m_play_button->set_icon_name("media-playback-start-symbolic");
            m_media_file->seek(0);
        });
        m_duration_connection = m_media_file->property_duration().signal_changed().connect([this]()
        {
            double seconds = (double)m_media_file->get_duration() / G_USEC_PER_SEC;
            setup_duration(seconds);
            m_duration_connection.disconnect();
        });
        m_media_file->property_prepared().signal_changed().connect([this]()
        {
            set_controls_visible(true, true);
            if (m_spinner)
            {
                m_spinner->unparent();
                delete m_spinner;
                m_spinner = nullptr;
            }
        });

        m_media_file->property_error().signal_changed().connect([this]()
        {
            g_critical("%s", m_media_file->get_error().what());
        });

        m_media_file->play();
        if (m_media_file->get_error())
        {
            g_critical("%s", m_media_file->get_error().what());
        }

        if (m_url_prep_thread)
        {
            m_url_prep_thread->join();
            delete m_url_prep_thread;
            m_url_prep_thread = nullptr;
        }
    }

    void VideoPlayer::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_video)
        {
            m_video->size_allocate(rect, baseline);
        }

        if (m_controls_revealer)
        {
            m_controls_revealer->size_allocate(rect, baseline);
        }

        if (m_spinner)
        {
            m_spinner->size_allocate(rect, baseline);
        }
    }

    void VideoPlayer::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& , int& ) const
    {
        if (m_video)
        {
            int childMinimum, childNatural, childMinBaseLine, childNatBaseline;
            m_video->measure(orientation, for_size, childMinimum, childNatural, childMinBaseLine, childNatBaseline);
            minimum = std::max(minimum, childMinimum);
            natural = std::max(natural, childNatural);
        }

        if (m_controls_revealer)
        {
            int childMinimum, childNatural, childMinBaseLine, childNatBaseline;
            m_controls_revealer->measure(orientation, for_size, childMinimum, childNatural, childMinBaseLine, childNatBaseline);
            minimum = std::max(minimum, childMinimum);
            natural = std::max(natural, childNatural);
        }

        if (m_spinner)
        {
            int childMinimum, childNatural, childMinBaseLine, childNatBaseline;
            m_spinner->measure(orientation, for_size, childMinimum, childNatural, childMinBaseLine, childNatBaseline);
            minimum = std::max(minimum, childMinimum);
            natural = std::max(natural, childNatural);
        }
    }

    void VideoPlayer::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_video)
        {
            snapshot_child(*m_video, snapshot);
        }

        if (m_controls_revealer)
        {
            snapshot_child(*m_controls_revealer, snapshot);
        }

        if (m_spinner)
        {
            snapshot_child(*m_spinner, snapshot);
        }
    }

    void VideoPlayer::play()
    {
        if (m_media_file && !m_media_file->get_playing())
        {
            m_media_file->play();
        }
    }

    void VideoPlayer::pause()
    {
        if (m_media_file && m_media_file->get_playing())
        {
            m_media_file->pause();
        }
    }
}
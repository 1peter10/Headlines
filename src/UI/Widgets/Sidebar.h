#pragma once

namespace ui
{
    class Sidebar
    {
    public:
        void OnAppActivate(const Glib::RefPtr<Gtk::Builder>& builder);
        void UpdateMenuItems();

    private:
        void UpdateUnreadMessagesLabel(unsigned int unreadCount);

        std::unordered_map<Gtk::ListBoxRow*, std::function<void()>> m_ItemCallbacks;

        Gtk::Widget* m_HeaderBar;
        Gtk::Button* m_MenuButton;

        Gtk::ListBoxRow* m_LoginItem;
        Gtk::ListBoxRow* m_ProfileItem;
        Gtk::ListBoxRow* m_MessagesItem;
        Gtk::Label* m_UnreadMessagesLabel;
        Gtk::ListBoxRow* m_LogoutItem;
        Gtk::ListBoxRow* m_LoginSeparator;
        Gtk::ListBoxRow* m_UpvotedItem;
        Gtk::ListBoxRow* m_DownvotedItem;
        Gtk::ListBoxRow* m_SavedItem;
        Gtk::ListBoxRow* m_GildedItem;
        Gtk::ListBoxRow* m_MultiredditItem;
        Gtk::ListBoxRow* m_HiddenItem;
        Gtk::ListBoxRow* m_UserSubredditsItem;
    };
}

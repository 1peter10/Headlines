#pragma once

#include "RedditContentProvider.h"

namespace ui
{
    class UserMultiRedditsContentProvider : public RedditContentProvider
    {
    public:
        UserMultiRedditsContentProvider();
        void SetItemClickHandler(std::function<void(const SearchResultWidget*)> onClick);
    private:
        virtual std::vector<RedditContentListItemRef> GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker) override;
        virtual std::string GetFailedToLoadErrorMsg() override;

        std::function<void(const SearchResultWidget*)> m_OnClick;
    };
}
#pragma once
#include "UI/ContentProviders/RedditContentProvider.h"
#include "API/api_fwd.h"

namespace ui
{
    class PostWidget;
    class UserPostsContentProvider : public RedditContentProvider
    {
    public:
        UserPostsContentProvider();
        void SetUser(const api::UserRef& user);
    private:
        virtual std::vector<RedditContentListItemRef> GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker) override;
        virtual std::string GetFailedToLoadErrorMsg() override;
        api::UserRef m_User;
    };
}

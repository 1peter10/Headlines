#pragma once
#include "RedditContentProvider.h"

namespace ui
{
    class PostWidget;
    class SearchPostsContentProvider : public RedditContentProvider
    {
    public:
        SearchPostsContentProvider(const ui::SearchPage* searchPage);

    private:
        const ui::SearchPage* m_Parent;
        virtual std::vector<RedditContentListItemRef> GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker) override;
        virtual std::string GetFailedToLoadErrorMsg() override;
    };
}

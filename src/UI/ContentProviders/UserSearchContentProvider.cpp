#include <AppSettings.h>

#include <utility>
#include "UserSearchContentProvider.h"
#include "UI/Widgets/SearchResultWidget.h"
#include "UI/Pages/SearchPage.h"
#include "API/RedditAPI.h"
#include "Application.h"

namespace ui
{
    UserSearchContentProvider::UserSearchContentProvider(const SearchPage* searchPage)
            : m_Parent(searchPage)
            , m_OnClick(nullptr)
    {

    }

    std::vector<RedditContentListItemRef> UserSearchContentProvider::GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker)
    {
        std::vector<api::UserRef> users = api::RedditAPI::Get()->GetSearchUsers(m_Parent->GetSearchString(), 20, lastTimeStamp);
        std::vector<RedditContentListItemRef> uiElements;
        for (const api::UserRef& user : users)
        {
            if (worker->IsCancelled())
            {
                return uiElements;
            }

            SearchResultWidgetRef postUI = std::make_shared<SearchResultWidget>(user, m_OnClick);
            uiElements.push_back(postUI);
        }

        return uiElements;
    }

    std::string UserSearchContentProvider::GetFailedToLoadErrorMsg()
    {
        return "No results found. Retry?";
    }

    void UserSearchContentProvider::SetItemClickHandler(std::function<void(const SearchResultWidget*)> onClick)
    {
        m_OnClick = std::move(onClick);
    }
}
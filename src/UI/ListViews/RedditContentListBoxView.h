#pragma once

#include "RedditContentListView.h"

namespace ui
{
    class RedditContentListBoxView : public RedditContentListView
    {
    public:
        explicit RedditContentListBoxView(const RedditContentProviderRef& contentProvider);
        ~RedditContentListBoxView();
        void CreateUI(Gtk::Box* parent, Gtk::Viewport* parentViewport) override;
        Gtk::ListBox* GetListBox() const { return m_Listbox; }
    private:
        void AddContentToUI(std::vector<RedditContentListItemRef> newContent) override;
        Gtk::ListBox* m_Listbox;
        int m_RoundedCornersSettingChangedHandler;
    };
}
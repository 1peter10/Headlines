#pragma once
namespace api
{
    class Post;
    typedef std::shared_ptr<Post> PostRef;

    class Comment;
    typedef std::shared_ptr<Comment> CommentRef;

    class Subreddit;
    typedef std::shared_ptr<Subreddit> SubredditRef;

    class User;
    typedef std::shared_ptr<User> UserRef;

    class Flair;
    typedef std::shared_ptr<Flair> FlairRef;

    class MultiReddit;
    typedef std::shared_ptr<MultiReddit> MultiRedditRef;

    class Message;
    typedef std::shared_ptr<Message> MessageRef;

    class Award;
    typedef std::shared_ptr<Award> AwardRef;
}


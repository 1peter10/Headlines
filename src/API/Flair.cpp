#include "Flair.h"
#include "util/Helpers.h"
#include "API/Post.h"

namespace api
{
    Flair::Flair(const Json::Value& jsonData)
    {
        unsigned int colour = util::Helpers::HexStringToUInt(jsonData["background_color"].asString());
        m_Colour = util::Colour(colour);
        m_Text = jsonData["text"].asString();
        m_ID = jsonData["id"].asString();
        m_TextColour = jsonData["text_color"] == "dark" ? util::Colour(0, 0, 0) : util::Colour(1, 1, 1);
    }

    Flair::Flair(const PostRef& post)
    {
        m_Colour = post->GetFlairColour();
        m_Text = post->GetFlairText();
        m_TextColour = post->GetFlairTextColour();
    }

    Flair::Flair(const std::string& text, const util::Colour& colour, const util::Colour& textColour)
        : m_Text(text)
        , m_Colour(colour)
        , m_TextColour(textColour)
    {

    }
}
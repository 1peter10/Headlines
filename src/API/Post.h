#pragma once

#include <util/Helpers.h>
#include "pch.h"
#include "UI/ListViews/RedditContentListView.h"
#include "util/Colour.h"
#include "API/Award.h"

namespace api
{
    class Post
    {
    public:
        explicit Post(const Json::Value& postJson);

        const std::string& GetTitle() const { return m_Title; }
        const std::string& GetSubreddit() const { return m_Subreddit; }
        const std::string& GetSubredditPrefixed() const { return m_SubredditPrefixed; }
        const std::string& GetUserName() const { return m_UserName; }
        const std::string& GetUserNamePrefixed() const { return m_UserNamePrefixed; }
        int GetThumbWidth() const { return m_ThumbWidth; }
        int GetThumbHeight() const { return m_ThumbHeight; }
        const std::string& GetFullID() const { return m_FullID; }
        const std::string& GetUrl() const { return m_Url; }
        const std::string& GetSelfText() const { return m_SelfText; }

        const std::vector<util::ImageCollection>& GetImagePaths() const { return m_ImagePaths; }
        bool IsVideo() const { return m_IsVideo; }
        const std::string& GetVideoUrl() { return m_VideoPath; }
        const std::string& GetCommentsLink() { return m_CommentsLink; }
        int GetScore() const { return m_Score; }
        int GetNumComments() const { return m_NumComments; }
        bool IsNSFW() const { return m_NSFW; }
        bool IsSpoiler() const { return m_Spoiler; }
        const std::string& GetPostHint() const { return m_PostHint; }
        bool IsGallery() const { return m_IsGallery; }
        const util::Colour& GetFlairColour() const { return m_FlairColour; }
        const std::string& GetFlairText() const { return m_FlairText; }
        const util::Colour& GetFlairTextColour() const { return m_FlairTextColour; }
        uint64_t GetTimeStamp() const { return m_Timestamp; }
        const std::vector<Award>& GetAwards() { return m_Awards; }

        bool IsUpVoted() const { return m_UpVoted; }
        bool IsDownVoted() const { return m_DownVoted; }
        bool IsBookmarked() const { return m_Bookmarked; }

        void UpVote();
        void RemoveUpVote();
        void DownVote();
        void RemoveDownVote();

        void Bookmark();
        void RemoveBookmark();

    private:
        std::string m_Title;
        std::string m_SelfText;
        std::vector<util::ImageCollection> m_ImagePaths;
        std::string m_VideoPath;
        std::string m_Subreddit;
        std::string m_SubredditPrefixed;
        std::string m_UserName;
        std::string m_UserNamePrefixed;
        std::string m_CommentsLink;
        std::string m_Url;
        std::string m_FullID;
        std::string m_PostHint;
        int m_ThumbHeight;
        int m_ThumbWidth;
        int m_Score;
        int m_NumComments;
        bool m_IsVideo;
        bool m_UpVoted;
        bool m_DownVoted;
        bool m_Bookmarked;
        bool m_Spoiler;
        bool m_NSFW;
        bool m_IsGallery;
        util::Colour m_FlairColour;
        std::string m_FlairText;
        util::Colour m_FlairTextColour;
        uint64_t m_Timestamp;
        std::vector<Award> m_Awards;
    };
}

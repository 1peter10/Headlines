#pragma once

namespace api
{
    enum class PostsType
    {
        Best,
        Controversial,
        Hot,
        New,
        Random,
        Rising,
        TopAllTime,
        TopThisYear,
        TopThisMonth,
        TopThisWeek,
        TopToday,
        TopNow
    };

    inline std::string PostsTypeToString(PostsType type)
    {
        switch (type)
        {
            case PostsType::Best:
            {
                return "Best";
            }
            case PostsType::Controversial:
            {
                return "Controversial";
            }
            case PostsType::Hot:
            {
                return "Hot";
            }
            case PostsType::New:
            {
                return "New";
            }
            case PostsType::Random:
            {
                return "Random";
            }
            case PostsType::Rising:
            {
                return "Rising";
            }
            case PostsType::TopAllTime:
            {
                return "Top: All Time";
            }
            case PostsType::TopThisYear:
            {
                return "Top: This Year";
            }
            case PostsType::TopThisMonth:
            {
                return "Top: This Month";
            }
            case PostsType::TopThisWeek:
            {
                return "Top: This Week";
            }
            case PostsType::TopToday:
            {
                return "Top: Today";
            }
            case PostsType::TopNow:
            {
                return "Top: Now";
            }
        }

        return "";
    }
}

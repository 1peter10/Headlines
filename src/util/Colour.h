#pragma once

namespace util
{
    class Colour
    {
    public:
        Colour();
        Colour(double r, double g, double b);
        explicit Colour(unsigned int hex);

        double GetR() const { return m_R; }
        double GetG() const { return m_G; }
        double GetB() const { return m_B; }

        Pango::AttrColor AsPangoForegroundColour() const;
        Pango::AttrColor AsPangoBackgroundColour() const;

        void SetR(double r) { m_R = r; }
        void SetG(double g) { m_G = g; }
        void SetB(double b) { m_B = b; }

    private:
        double m_R;
        double m_G;
        double m_B;
    };
}

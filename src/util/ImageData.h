#pragma once

namespace util
{
    enum class ImageQuality
    {
        Lowest,
        Low,
        Medium,
        High,
        Highest
    };

    class ImageData
    {
    public:
        ImageData();
        explicit ImageData(const std::string& imageUrl, int width = -1, int height = -1);
        std::string GetFSPath() const;

        std::string m_Url;
        int m_Width = -1;
        int m_Height = -1;
    };

    class ImageCollection
    {
    public:
        const ImageData& GetImageData(ImageQuality quality) const;
        std::vector<ImageData> m_Images;
    };
}

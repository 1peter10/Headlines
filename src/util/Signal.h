#pragma once

namespace util
{
    static constexpr int s_InvalidSignalHandlerID = -1;
    static int s_CurrSignalHandlerID = 0;
    template<typename... T>
    class SignalHandler
    {
    public:
        explicit SignalHandler(std::function<void(T...)> handler) : m_ID(s_CurrSignalHandlerID++), m_Handler(handler) {}
        int m_ID;
        std::function<void(T...)> m_Handler;
    private:
    };


    template<typename... T>
    class Signal
    {
    public:
        int AddHandler(std::function<void(T...)> handler);
        void RemoveHandler(int id);
        void Fire(T... args);

    private:
        std::vector<SignalHandler<T...>> m_Handlers;
    };

    template<typename... T>
    int Signal<T...>::AddHandler(std::function<void(T...)> handler)
    {
        m_Handlers.push_back(SignalHandler<T...>(handler));
        return m_Handlers.back().m_ID;
    }

    template<typename... T>
    void Signal<T...>::RemoveHandler(int id)
    {
        m_Handlers.erase(std::remove_if(m_Handlers.begin(), m_Handlers.end(), [id](const SignalHandler<T...> handler) { return handler.m_ID == id; }));
    }

    template<typename... T>
    void Signal<T...>::Fire(T... args)
    {
        for (SignalHandler<T...> handler : m_Handlers)
        {
            handler.m_Handler(args...);
        }
    }
}
